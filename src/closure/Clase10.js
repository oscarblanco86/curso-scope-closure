export function createPetList() {
    let pets = []
    return function addPets(pet) {
      if (pet) {
        pets.push(pet);
      }
      return pets;
    }
  }

  const myPetList = createPetList();

// myPetList("michi");

// myPetList("firulais");

// myPetList();

myPetList({"age": 3, "description": "A nice cat", "name": "Michi", "type": "cat", "weight": "2kg"});
myPetList({"age": 6, "description": "A good dog", "name": "firulais", "type": "dog", "weight": "15kg"});
myPetList();

