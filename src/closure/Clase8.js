export function sumWithClosure(firstNum) {
    // Tu código aquí 👈
    return function mySuma(secondNum) {
      let a = firstNum ?? 0;
      let b = secondNum ?? 0;
      return a + b;
    }
  }