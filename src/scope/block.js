function fruits() {
    //todo lo que este dentro de handle bars es un bloque
    if (true) {
        var fruit1 = 'Apple'; //function scope
        let fruit2 = 'Kiwi'; //Block scope
        const fruit3 = 'Banana'; //Block scope    
        console.log(fruit2);
        console.log(fruit3);
    }
    console.log(fruit1);

}

fruits();