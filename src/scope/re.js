var firstName; // Undefined
firstName = 'Oscar';
console.log(firstName);

var lastName = 'Blanco';
lastName = 'Ana';
console.log(lastName);

var secondName = 'Mauricio';
var secondName = 'Ana';
console.log(secondName);

//let

let fruit = 'Apple';
fruit = 'kiwi';
console.log(fruit);

// let fruit = 'Banana'; // Da error porque ya existe una declaracion LET en la linea 15
// console.log(fruit);

//const
const animal = 'dog';
// // animal = 'cat';
// const animal = 'snake';// Da error de canno redeclare varaible
console.log(animal);

const vehicles = [];
vehicles.push('🚗');
console.log(vehicles);

vehicles.pop();
console.log(vehicles);